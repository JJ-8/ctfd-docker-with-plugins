# ctfd-docker-with-plugins

## Name

CTFd container with plugins

## Description

This repo builds a CTFd container image from CTFD/ctfd and assorted plugin sources.
You could customize this for your workflows.

## Workflows

Basic workflow:

- clone the repo
- make adaptations
- `docker build -t ctfd-jcr .`
- prepare an environment file for the `initial_setup` plugin
- `docker run -p 8000:8000 -it --rm --env-file env -t ctfd-jcr`

This will show you a local test environment, probably without container challenge support, though you should be able to review the user interface for container challenges.

Kubernetes based (developer) workflow:

- clone the repo
- login to your favorite docker registry
- make adaptations
- `docker build -t <registry name and image tag> .`
- `docker push <registry name and image tag>`
- prepare an environment file for the `initial_setup` plugin
- create a relevant Kubernetes manifest (coming up real soon)
- `kubectl apply -f manifest.yml`

Note that this is a developer workflow, not a production workflow. You may also want to consider using branches.

Kubernetes based acceptance test and production worklow (sketch):

- work in a branch
- make adaptions
- commit
- the CI/CD pipeline will create an image in the gitlab repo with an appropriate tag
- adapt the production Kubernetest manifests to refer to this image. For example <https://gitlab.com/jointcyberrange.nl/jcr-deploy-by-argo-cd-do>.
- when done, create a merge request

Deployment of challenges via `ctfcli`: see <https://gitlab.com/jointcyberrange.nl/ctfcli-testchallenges>.

This could potentially fail due to certificate issues.

## Boiler plate

What follows is from the gitlab template, and serves as a reminder and todo list.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
