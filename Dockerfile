# syntax=docker/dockerfile:1
# the line before is required for git references, it is not a comment

# Stage 1: Build stage with git to download CTFd plugins using Git
FROM alpine/git AS git

WORKDIR /tmp

# Clone the repository and copy the required directories
# RUN git clone --branch main --single-branch https://gitlab.com/jointcyberrange.nl/ctfd-jcr.git && \
#     mkdir -p /tmp/plugins && \
#     cp -r /tmp/ctfd-jcr/CTFd/plugins/initial_setup /tmp/plugins/initial_setup && \
#     cp -r /tmp/ctfd-jcr/CTFd/plugins/container_challenges /tmp/plugins/container_challenges
#     cp -r /tmp/ctfd-jcr/CTFd/plugins/custom_auth /tmp/plugins/custom_auth

# Stage 2: Final stage with CTFd as runtime image
# Use the CTFd base image; tag older versions if needed. e.g. ctfd/ctfd:3.6.1
FROM ctfd/ctfd:latest AS runtime

# Ensure running as root for package update
USER root

# Copy plugins from git build phase
# COPY --from=git /tmp/plugins /opt/CTFd/CTFd/plugins

# Local development
COPY ./initial_setup /opt/CTFd/CTFd/plugins/initial_setup
COPY ./container_challenges /opt/CTFd/CTFd/plugins/container_challenges
# COPY ./custom_auth /opt/CTFd/CTFd/plugins/custom_auth

# # Update packages and install pip
RUN apt-get update

# Switch to runtime user
USER ctfd

# docker build --no-cache if there are changes in the repo. 
WORKDIR /opt/CTFd

RUN pip install --no-cache-dir -r requirements.txt \
    && for d in CTFd/plugins/*; do \
        if [ -f "$d/requirements.txt" ]; then \
            pip install --no-cache-dir -r "$d/requirements.txt";\
        fi; \
    done;

# where do we run all the test cases? We need a smoke test on the container image. Should also be in the CICD pipeline.
# maybe docker run make test or something

EXPOSE 8000

# docker build -t ctfd-jcr .
# docker run -p 8000:8000 -it --rm --env-file env -t ctfd-jcr
ENTRYPOINT ["/opt/CTFd/docker-entrypoint.sh"]
